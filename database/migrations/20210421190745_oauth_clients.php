<?php

use think\migration\Migrator;
use think\migration\db\Column;

class OauthClients extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('oauth_clients', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci', 'comment' => '' ,'id' => 'id','signed' => true ,'primary_key' => ['id']]);
        $table
            ->addColumn('client_id', 'string', ['limit' => 30,'null' => false,'signed' => false,'comment' => '',])
            ->addColumn('client_secret', 'string', ['limit' => 200,'null' => false,'signed' => false,'comment' => '',])
            ->addColumn('name', 'string', ['limit' => 191,'null' => false,'default' => null,'signed' => false,'comment' => '',])
            ->addColumn('resource_ids', 'text', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_MEDIUM,'null' => true,'signed' => false,'comment' => '',])
            ->addColumn('scope', 'string', ['limit' => 255,'null' => true,'signed' => false,'comment' => '',])
            ->addColumn('authorized_grant_types', 'string', ['limit' => 255, 'null' => false,'default' => null,'signed' => false,'comment' => '',])
            ->addColumn('redirect_uri', 'string', ['limit' => 255, 'null' => true,'default' => null,'signed' => false,'comment' => '',])
            ->addColumn('access_token_validity', 'integer', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::INT_MEDIUM, 'null' => false,'default' => null,'signed' => false,'comment' => '',])
            ->addColumn('refresh_token_validity', 'integer', ['limit' => \Phinx\Db\Adapter\MysqlAdapter::INT_MEDIUM, 'null' => false,'default' => null,'signed' => false,'comment' => '',])
            ->addColumn('created_at', 'timestamp', ['null' => true,'signed' => false,'comment' => '',])
            ->addColumn('updated_at', 'timestamp', ['null' => true,'signed' => false,'comment' => '',])
            ->addIndex(['client_id'], ['unique' => true, 'name' => 'oauth_clients_client_id_unique'])
            ->create();
    }
}
