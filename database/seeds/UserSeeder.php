<?php

use think\migration\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        \app\model\User::create([
            "name" => "Hamid Atyabi",
            "email" => "atyabi.hamid@yahoo.com",
            "password" => md5("123456"),
        ]);
    }
}