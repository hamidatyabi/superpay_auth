<?php

use think\migration\Seeder;
use \app\contracts\Scopes;
class ClientSeeder extends Seeder
{

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        \app\model\Client::create([
            "client_id" => "2db3dae0147644d856b4e570867896",
            "client_secret" => "7ceab3ac0494cca16dc0fa5d04f3ba60",
            "name" => "API Client",
            "resource_ids" => null,
            "scope" => implode(",", [Scopes::USER_READ_PRIVATE]),
            "authorized_grant_types" => "authorization_code,password,refresh_token,client_credentials",
            "redirect_uri" => null,
            "access_token_validity" => 1200,
            "refresh_token_validity" => (60 * 60 * 24 * 7 * 365),
        ]);
    }
}