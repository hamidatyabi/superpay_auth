<?php


namespace app\repositories\UserRepositories;


use app\contracts\Mail;
use app\exceptions\EndpointException;
use app\model\User;
use app\repositories\CacheRepositories\CacheRepository;
use app\Response;
use think\facade\Db;

class UserRepositoryMysql implements UserRepositoryInterface
{
    protected User $model;
    protected CacheRepository $cacheRepository;
    /**
     * @param User $model
     * @param CacheRepository $cacheRepository
     * UserRepositoryMysql constructor.
     */
    public function __construct(User $model, CacheRepository $cacheRepository)
    {
        $this->model = $model;
        $this->cacheRepository = $cacheRepository;
    }

    public function findByEmail(string $email){
        if($cache = $this->cacheRepository->get("USER@$email")) return $this->findById($cache);
        if($result = $this->model->where('email', $email)->limit(1)->find()) {
            return $this->storeToCache($result);
            return $result;
        }
        return null;
    }

    public function findById($id){
        if($cache = $this->cacheRepository->get("USER:$id")) return $cache;
        if($result = $this->model->where('id', $id)->limit(1)->find()) {
            return $this->storeToCache($result);
        }
        return null;
    }

    public function credential(string $username, string $password){
        $user = $this->findByEmail($username);
        if(!$user) throw new EndpointException("unauthorized", "Bad credentials", Response::STATUS_UNAUTHORIZED);
        if($user->password != md5($password)) throw new EndpointException("unauthorized", "Bad credentials", Response::STATUS_UNAUTHORIZED);
        if(!$user->email_verified_at) throw new EndpointException("unauthorized", "Your account is not active", Response::STATUS_UNAUTHORIZED);
        return $user;
    }

    private function storeToCache($user){
        $this->cacheRepository->set("USER:".$user->id, $user);
        $this->cacheRepository->set("USER@".$user->email, $user->id);
        return $user;
    }

    private function clearCache($user){
        $this->cacheRepository->delete("USER:".$user->id);
        $this->cacheRepository->delete("USER@".$user->email);
    }

    public function createUserAccount($data){
        $this->model->startTrans();
        try {
            if(env('MAIL_HOST') == "")
                $data['email_verified_at'] = date("Y-m-d H:i:s");
            $user = $this->model->create($data);
            if(env('MAIL_HOST') != ""){
                $code = md5(mt_rand(111111111, 999999999));

                $link = "http://localhost:8000".url('user-verification', ['code' => $code])->build();
                Mail::send("Active your account", $data['email'], '
            <h1>Hi '.$data['name'].'</h1>
            <p>You have to active account by click on button</p>
            <a href="'.$link.'" style="border: 1px solid #ccc; padding: 10px 20px; background: #ccc">Activation URL</a>'
                );
                $this->cacheRepository->set("VERIFY:$code", $user, 3600);
            }
            $this->model->commit();
            return $user;
        }catch (\EndpointException $exception){
            throw $exception;
        }
        $this->model->rollback();
        return null;
    }
    public function updateUserAccount($data, $user){
        $this->model->where('id', $user->id)->save($data);
        $this->clearCache($user);
    }
    public function checkActivationCode($code){
        if($cache = $this->cacheRepository->get("VERIFY:$code")) {
            $this->cacheRepository->delete("VERIFY:$code");
            return $cache;
        }
        return false;
    }

}