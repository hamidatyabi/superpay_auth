<?php


namespace app\repositories\UserRepositories;


interface UserRepositoryInterface
{
    public function findByEmail(string $email);

    public function findById($id);

    public function credential(string $username, string $password);

    public function createUserAccount($data);

    public function updateUserAccount($data, $user);

    public function checkActivationCode($code);
}