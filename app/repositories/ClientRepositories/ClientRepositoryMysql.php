<?php


namespace app\repositories\ClientRepositories;


use app\exceptions\EndpointException;
use app\model\Client;
use app\repositories\CacheRepositories\CacheRepository;
use app\Response;

class ClientRepositoryMysql implements ClientRepositoryInterface
{
    protected Client $model;
    protected CacheRepository $cacheRepository;
    /**
     * @param Client $model
     * @param CacheRepository $cacheRepository
     * ClientRepositoryMysql constructor.
     */
    public function __construct(Client $model, CacheRepository $cacheRepository)
    {
        $this->model = $model;
        $this->cacheRepository = $cacheRepository;
    }

    public function findByClientId(string $client_id){
        if($cache = $this->cacheRepository->get("CLIENT:$client_id")) return $cache;
        if($result = $this->model->where('client_id', $client_id)->limit(1)->find()) {
            $this->cacheRepository->set("CLIENT:$client_id", $result);
            return $result;
        }
        return null;
    }
    public function credential(string $client_id, string $client_secret){
        $client = $this->findByClientId($client_id);
        if(!$client) throw new EndpointException("unauthorized", "Bad client credentials", Response::STATUS_UNAUTHORIZED);
        if($client_secret != $client->client_secret) throw new EndpointException("unauthorized", "Bad client credentials", Response::STATUS_UNAUTHORIZED);
        return $client;
    }

    public function storeAuthorizeCode($code){
        $this->cacheRepository->set("CODE:".$code['code'], $code, 120);
        return $code['encrypted'];
    }

    public function getAuthorizeCode($code){
        return $this->cacheRepository->get("CODE:$code");
    }

    public function delAuthorizeCode($code){
        return $this->cacheRepository->delete("CODE:$code");
    }

}