<?php


namespace app\repositories\ClientRepositories;


interface ClientRepositoryInterface
{
    public function findByClientId(string $client_id);

    public function credential(string $client_id, string $client_secret);

    public function storeAuthorizeCode($code);

    public function getAuthorizeCode($code);

    public function delAuthorizeCode($code);
}