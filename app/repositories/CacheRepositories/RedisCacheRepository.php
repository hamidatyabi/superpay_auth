<?php


namespace app\repositories\CacheRepositories;


use think\cache\driver\Redis;

class RedisCacheRepository implements CacheRepository
{
    protected Redis $redis;
    /**
     * RedisCacheRepository constructor.
     */
    public function __construct()
    {
        $options = [
            'host'       => env('REDIS_HOST', '127.0.0.1'),
            'port'       => env('REDIS_PORT', 6379),
            'password'   => env('REDIS_PASSWORD', ''),
            'select'     => env('REDIS_BD', 0),
        ];
        $this->redis = new Redis($options);
    }

    public function set($key, $value, $expire = null) : bool
    {
        if(is_object($value) || is_array($value)) $value = json_encode($value);
        return $this->redis->set($key, $value, $expire);
    }

    public function get($key)
    {
        if($this->redis->has($key)){
            $value = $this->redis->get($key);
            if(isJson($value)) return json_decode($value);
            return $value;
        }
        return null;
    }

    public function delete($key)
    {
        return $this->redis->delete($key);
    }


}