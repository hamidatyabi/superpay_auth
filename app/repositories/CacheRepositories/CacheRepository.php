<?php


namespace app\repositories\CacheRepositories;


interface CacheRepository
{
    public function set($key, $value, $expire = null) : bool;

    public function get($key);

    public function delete($key);
}