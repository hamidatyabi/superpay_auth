<?php
declare (strict_types = 1);

namespace app\middleware;

class AuthenticationMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        if(!session('auth_user_id')){
            return \redirect(url('login', ['continue' => base64encode($request->url())])->build());
        }
        return $next($request);
    }
}
