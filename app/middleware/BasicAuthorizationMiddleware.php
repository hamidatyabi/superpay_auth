<?php
declare (strict_types = 1);

namespace app\middleware;

use app\contracts\Principle;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\Request;
use app\Response;

class BasicAuthorizationMiddleware
{
    protected ClientRepositoryInterface $clientRepository;
    /**
     * @param ClientRepositoryInterface $clientRepository
     * BasicAuthorizationMiddleware constructor.
     */
    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle(Request $request, \Closure $next)
    {
        try {
            if(!isset($_SERVER['PHP_AUTH_USER'])) throw new EndpointException("unauthorized", "", Response::STATUS_UNAUTHORIZED);
            $client_id = $_SERVER['PHP_AUTH_USER'];
            $client_secret = $_SERVER['PHP_AUTH_PW'];
            $client = $this->clientRepository->credential($client_id, $client_secret);
            $request->setPrinciple(new Principle($client));
            return $next($request);

        }catch (EndpointException $endpointException){
            return Response::json($endpointException->getError(), $endpointException->getStatus());
        }
    }
}
