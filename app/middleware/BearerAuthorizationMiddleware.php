<?php
declare (strict_types = 1);

namespace app\middleware;

use app\contracts\JwtAuth;
use app\contracts\Principle;
use app\contracts\ScopeAccess;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\Request;
use app\Response;

class BearerAuthorizationMiddleware
{
    protected ClientRepositoryInterface $clientRepository;
    protected UserRepositoryInterface $userRepository;
    /**
     * @param ClientRepositoryInterface $clientRepository
     * @param UserRepositoryInterface $userRepository
     * BearerAuthorizationMiddleware constructor.
     */
    public function __construct(ClientRepositoryInterface $clientRepository, UserRepositoryInterface $userRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * 处理请求
     *
     * @param Request $request
     * @param \Closure  $next
     * @param $scope
     * @param $access
     * @return Response
     */
    public function handle(Request $request, \Closure $next, $scope = null, $access = [])
    {
        try {
            $access_token = getBearerToken();
            if(!$access_token) throw new EndpointException("unauthorized", "Full authentication is required to access this resource", Response::STATUS_UNAUTHORIZED);
            $jwt = new JwtAuth();
            $token = $jwt->decode($access_token);
            // check token validity
            if(!$token)
                throw new EndpointException("invalid_token", "Cannot convert access token to JSON", Response::STATUS_UNAUTHORIZED);

            // if there is ati in token it' mean it's refresh token and user can't access to endpoint
            if(property_exists($token, "ati"))
                throw new EndpointException("invalid_token", "Encoded token is a refresh token", Response::STATUS_UNAUTHORIZED);

            // check scope for access to endpoint
            if($scope && !in_array($scope, $token->scope))
                throw new EndpointException("access_denied", "Your token doesn't have access to this scope", Response::STATUS_ACCESS_DENIED);

            // check client of token
            $client = $this->clientRepository->findByClientId($token->client_id);
            if(!$client)
                throw new EndpointException("invalid_token", "Client token is invalid", Response::STATUS_UNAUTHORIZED);

            if(property_exists($token, "user_id")) {
                $user = $this->userRepository->findById($token->user_id);
                $request->setPrinciple(new Principle($client, $token, $user));
            }else // if token be a clientCredentials
                $request->setPrinciple(new Principle($client, $token));

            if($access && in_array(ScopeAccess::USER, $access) && !$request->getPrinciple()->getUser())
                throw new EndpointException("access_denied", "Encoded token is a client_credentials", Response::STATUS_ACCESS_DENIED);

            if($access && in_array(ScopeAccess::CLIENT, $access) && $request->getPrinciple()->getUser())
                throw new EndpointException("access_denied", "Encoded token is a user token not client_credentials", Response::STATUS_ACCESS_DENIED);

            return $next($request);

        }catch (EndpointException $unauthorizedException){
            return Response::json($unauthorizedException->getError(), $unauthorizedException->getStatus());
        }
    }

}
