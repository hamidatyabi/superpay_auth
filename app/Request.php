<?php
namespace app;

use app\contracts\Principle;

class Request extends \think\Request
{
    protected Principle $principle;

    /**
     * @return Principle
     */
    public function getPrinciple() : Principle
    {
        return $this->principle;
    }

    /**
     * @param Principle $principle
     */
    public function setPrinciple(Principle $principle): void
    {
        $this->principle = $principle;
    }

}
