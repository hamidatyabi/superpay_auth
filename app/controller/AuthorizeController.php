<?php
namespace app\controller;

use app\BaseController;
use app\contracts\AuthorizeCodeBuilder;
use app\contracts\JwtAuth;
use app\contracts\Principle;
use app\contracts\User;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\Request;
use app\validate\AuthorizeValidate;
use app\validation;
use app\Response;
use think\exception\ValidateException;

class AuthorizeController extends BaseController
{
    protected UserRepositoryInterface $userRepository;
    protected ClientRepositoryInterface $clientRepository;
    /**
     * @param UserRepositoryInterface $userRepository
     * @param ClientRepositoryInterface $clientRepository
     * TokenController constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository, ClientRepositoryInterface $clientRepository)
    {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
    }

    public function authorize(Request $request)
    {
        try{
            $this->validate($request->get(), AuthorizeValidate::class, []);
            $response_type = underscoreToCamelCase($request->get('response_type'));
            if(!method_exists($this, $response_type)) throw new ValidateException("Unsupported response type");
            $client = $this->clientRepository->findByClientId($request->get('client_id'));
            if(!$client)
                throw new EndpointException("invalid_client", "Invalid client id");
            if($client->redirect_uri != $request->get('redirect_uri'))
                throw new EndpointException("invalid_client", "Invalid redirect uri");

            $request->setPrinciple(new Principle($client));

            return $this->$response_type($request);
        }catch (ValidateException $validateException){
            return Response::json(['error' => "data_validation_failed", 'error_description' => $validateException->getError()], Response::STATUS_BAD_REQUEST);
        }catch (EndpointException $endpointException){
            return Response::json($endpointException->getError(), $endpointException->getStatus());
        }
        return Response::json(["error" => "Unknown_exception", "error_description" => 'Unknown exception'], Response::STATUS_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @return mixed
     *
     */
    protected function code(Request $request){
        $code = AuthorizeCodeBuilder::code($request->get('scope'), $request->getPrinciple()->getClient());
        $url = create_query_url($request->getPrinciple()->getClient()->redirect_uri, [
            "code" => $this->clientRepository->storeAuthorizeCode($code)
        ]);
        return redirect($url);
    }

    /**
     * @param Request $request
     * @return mixedK
     *
     */
    protected function token(Request $request){
        $credential = $this->userRepository->findById(session('auth_user_id'));
        $jwtAuth = new JwtAuth();
        $token = $jwtAuth->build($request->post("scope"), $request->getPrinciple()->getClient(), false, $credential);
        $url = create_query_url($request->getPrinciple()->getClient()->redirect_uri, [
            'access_token' => $token['access_token'],
            'token_type' => $token['token_type'],
            'expires_in' => $token['expires_in'],
        ]);
        return redirect($url);
    }
}
