<?php
namespace app\controller;

use app\BaseController;
use app\contracts\AuthorizeCodeBuilder;
use app\contracts\GrantTypes;
use app\contracts\JwtAuth;
use app\contracts\User;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\Request;
use app\validate\AuthorizeCodeValidate;
use app\validate\PasswordTokenValidate;
use app\validate\TokenValidate;
use app\validation;
use app\Response;
use think\exception\ValidateException;

class TokenController extends BaseController
{
    protected UserRepositoryInterface $userRepository;
    protected ClientRepositoryInterface $clientRepository;
    /**
     * @param UserRepositoryInterface $userRepository
     * @param ClientRepositoryInterface $clientRepository
     * TokenController constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository, ClientRepositoryInterface $clientRepository)
    {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
    }

    public function token(Request $request)
    {
        try{
            $this->validate($request->post(), TokenValidate::class, [], true);
            $grant_type = underscoreToCamelCase($request->post('grant_type'));
            if(!method_exists($this, $grant_type)) throw new ValidateException("Unsupported grant type");
            return $this->$grant_type($request);
        }catch (ValidateException $validateException){
            return Response::json(['error' => "data_validation_failed", 'error_description' => $validateException->getError()], Response::STATUS_BAD_REQUEST);
        }catch (EndpointException $endpointException){
            return Response::json($endpointException->getError(), $endpointException->getStatus());
        }
        return Response::json(["error" => "Unknown_exception", "error_description" => 'Unknown exception'], Response::STATUS_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param Request $request
     * @return mixed
     * get access_token and refresh token by email and password
     */
    protected function password(Request $request){
        $this->validate($request->post(), PasswordTokenValidate::class, [], true);

        $credential = $this->userRepository->credential($request->post("email"), $request->post("password"));
        $jwtAuth = new JwtAuth();
        $token = $jwtAuth->build($request->post("scope"), $request->getPrinciple()->getClient(), true, $credential);
        $response = array_merge($token, array(
            "user_id" => $credential->id,
            "email" => $credential->email
        ));
        return Response::json($response);
    }

    /**
     * @param Request $request
     * @return mixed
     * check authorization code and get access token and refresh token
     */

    protected function authorizationCode(Request $request){
        $this->validate($request->post(), AuthorizeCodeValidate::class, [], true);
        $authorize_code = AuthorizeCodeBuilder::decode($request->post('code'));
        if(!$authorize_code)
            throw new EndpointException("invalid_grant", 'Authorization code invalid');

        $code = $this->clientRepository->getAuthorizeCode($authorize_code->code);
        if(!$code)
            throw new EndpointException("invalid_grant", 'Authorization code expired');

        $this->clientRepository->delAuthorizeCode($authorize_code->code);
        $credential = $this->userRepository->findById($code->user_id);
        $jwtAuth = new JwtAuth();
        $token = $jwtAuth->build($request->post("scope"), $request->getPrinciple()->getClient(), true, $credential);
        $response = array_merge($token, array(
            "user_id" => $credential->id,
            "email" => $credential->email
        ));
        return Response::json($response);
    }

    /**
     * @param Request $request
     * @return mixed
     * generate new access and refresh token by refresh_token that user got in token api
     */
    protected function refreshToken(Request $request){
        $jwt = new JwtAuth();
        $token = $jwt->decode($request->post('refresh_token'));
        if(!$token)
            throw new EndpointException("invalid_token", "Cannot convert access token to JSON", Response::STATUS_UNAUTHORIZED);
        if(!property_exists($token, "ati"))
            throw new EndpointException("invalid_token", "Encoded token is a access token", Response::STATUS_UNAUTHORIZED);

        $credential = $this->userRepository->findById($token->user_id);
        $jwtAuth = new JwtAuth();
        $token = $jwtAuth->build($request->post("scope"), $request->getPrinciple()->getClient(), true, $credential);
        $response = array_merge($token, array(
            "user_id" => $credential->id,
            "email" => $credential->email
        ));
        return Response::json($response);
    }

    /**
     * @param Request $request
     * @return mixed
     * generate access token by client credentials user can access to endpoint that has permission to client
     */
    protected function clientCredentials(Request $request){
        $jwtAuth = new JwtAuth();
        $token = $jwtAuth->build($request->post("scope"), $request->getPrinciple()->getClient(), false);
        return Response::json($token);
    }
}
