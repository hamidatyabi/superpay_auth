<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\contracts\JwtAuth;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\Response;
use app\validate\PasswordTokenValidate;
use app\validate\TokenValidate;
use GuzzleHttp\Client;
use think\exception\ValidateException;
use think\facade\View;
use think\Request;
use think\Route;

class AccountController extends BaseController
{
    protected UserRepositoryInterface $userRepository;
    protected ClientRepositoryInterface $clientRepository;
    /**
     * @param UserRepositoryInterface $userRepository
     * @param ClientRepositoryInterface $clientRepository
     * AccountController constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository, ClientRepositoryInterface $clientRepository)
    {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function login(Request $request)
    {
        $data = [
            'continue' => $request->get('continue')
        ];
        if($request->isPost()){
            try{
                $this->validate($request->post(), PasswordTokenValidate::class, [], true);
                $credential = $this->userRepository->credential($request->post("email"), $request->post("password"));
                session('auth_user_id', $credential->id);
                if($continue = $request->get('continue')){
                    return \redirect(url(base64decode($continue))->build());
                }
                return \redirect(url('user-profile')->build());
            }catch (ValidateException $validateException){
                $data['error'] = json_encode($validateException->getError());
            }catch (EndpointException $endpointException){
                $data['error'] = $endpointException->getError()['error_description'];
            }
        }
        View::assign($data)->fetch('login');
        return view('login');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function profile(Request $request)
    {
        $data = [
            'user' => $this->userRepository->findById(session('auth_user_id'))
        ];

        View::assign($data)->fetch('profile');
        return view('profile');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        session('auth_user_id', null);
        return \redirect(url('login')->build());
    }


}
