<?php
namespace app\controller;

use app\BaseController;
use app\contracts\AuthorizeCodeBuilder;
use app\contracts\GrantTypes;
use app\contracts\JwtAuth;
use app\contracts\Mail;
use app\contracts\User;
use app\exceptions\EndpointException;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\Request;
use app\validate\AuthorizeCodeValidate;
use app\validate\PasswordTokenValidate;
use app\validate\RegisterValidate;
use app\validate\TokenValidate;
use app\validation;
use app\Response;
use think\exception\ValidateException;

class RegisterController extends BaseController
{
    protected UserRepositoryInterface $userRepository;
    protected ClientRepositoryInterface $clientRepository;
    /**
     * @param UserRepositoryInterface $userRepository
     * @param ClientRepositoryInterface $clientRepository
     * RegisterController constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository, ClientRepositoryInterface $clientRepository)
    {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
    }

    public function register(Request $request)
    {
        try{
            $this->validate($request->post(), RegisterValidate::class, [], true);
            $user = $this->userRepository->findByEmail($request->post('email'));
            if($user)
                throw new EndpointException("invalid_email", "The email already exist");
            $user = $this->userRepository->createUserAccount([
                "name" => $request->post('name'),
                "email" => $request->post("email"),
                "password" => md5($request->post("password"))
            ]);
            return Response::json($user);
        }catch (ValidateException $validateException){
            return Response::json(['error' => "data_validation_failed", 'error_description' => $validateException->getError()], Response::STATUS_BAD_REQUEST);
        }catch (EndpointException $endpointException){
            return Response::json($endpointException->getError(), $endpointException->getStatus());
        }
        return Response::json(["error" => "Unknown_exception", "error_description" => 'Unknown exception'], Response::STATUS_INTERNAL_SERVER_ERROR);
    }


    public function verification(Request $request)
    {
        $user = $this->userRepository->checkActivationCode($request->get('code'));
        if(!$user)
            return abort(404);
        $this->userRepository->updateUserAccount([
            "email_verified_at" => date("Y-m-d H:i:s")
        ], $user);
        session('auth_user_id', $user->id);
        return \redirect(url('user-profile')->build());
    }


}
