<?php
declare (strict_types = 1);

namespace app\controller;

use think\Request;
use think\facade\View;
class EndpointsController
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        View::assign([])->fetch('index');
        return view('index');
    }

}
