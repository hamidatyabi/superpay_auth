<?php
namespace app\controller;

use app\BaseController;
use app\contracts\Scopes;
use app\contracts\User;
use app\exceptions\EndpointException;
use app\Request;
use app\validate\TokenValidate;
use app\validation;
use app\Response;
use think\exception\ValidateException;

class ProfileController extends BaseController
{
    public function me(Request $request)
    {
        try{
            $user = $request->getPrinciple()->getUser();
            $user_info = array(
                "id" => $user->id,
                "name" => $user->name,
                "created_at" => $user->created_at,
                "email_verified_at" => $user->email_verified_at,
                "updated_at" => $user->updated_at
            );
            if(in_array(Scopes::USER_READ_PRIVATE, $request->getPrinciple()->getToken()->scope)){
                $user_info['email'] = $user->email;
            }
            return Response::json($user_info);
        }catch (EndpointException $endpointException){
            return Response::json($endpointException->getError(), $endpointException->getStatus());
        }
        return Response::json(["error" => "Unknown_exception", "error_description" => 'Unknown exception'], Response::STATUS_INTERNAL_SERVER_ERROR);
    }


}
