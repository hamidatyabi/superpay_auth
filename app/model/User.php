<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class User extends Model
{
    protected $table = "users";
    protected $createTime = "created_at";
    protected $updateTime = "updated_at";
}
