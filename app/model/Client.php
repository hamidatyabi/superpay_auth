<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Client extends Model
{
    protected $table = "oauth_clients";
    protected $createTime = "created_at";
    protected $updateTime = "updated_at";
}
