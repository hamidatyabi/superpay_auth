<?php


namespace app\contracts;


class GrantTypes
{
    public const AUTHORIZATION_CODE = "authorization_code";
    public const PASSWORD = "password";
    public const REFRESH_TOKEN = "refresh_token";
    public const CLIENT_CREDENTIALS = "client_credentials";


    public static function get(){
        return [
            self::AUTHORIZATION_CODE,
            self::PASSWORD,
            self::REFRESH_TOKEN,
            self::CLIENT_CREDENTIALS,
        ];
    }
}