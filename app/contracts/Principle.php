<?php


namespace app\contracts;


class Principle
{
    protected $client;
    protected $user;
    protected $token;

    /**
     * Principle constructor.
     * @param $client
     * @param $token
     * @param $user
     */
    public function __construct($client, $token = null, $user = null)
    {
        $this->client = $client;
        $this->token = $token;
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client): void
    {
        $this->client = $client;
    }/**
     * @return mixed|null
     */
    public function getToken()
    {
        return $this->token;
    }/**
     * @param mixed|null $token
     */
    public function setToken(?mixed $token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }


    public function toJson(): string
    {
        return json_encode([
            "client" => $this->client,
            "user" => $this->user,
            "token" => $this->token
        ]);
    }

}