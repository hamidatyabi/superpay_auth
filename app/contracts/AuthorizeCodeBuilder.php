<?php


namespace app\contracts;


class AuthorizeCodeBuilder
{
    public static function code($scope, $client){
        $scope = str_replace(", ", ",", $scope);
        $scopes = ($scope && $scope != "")?explode(",", $scope):[];
        $code = array(
            "code" => mt_rand(11111111,99999999)."".time(),
            "client_id" => $client->client_id,
            "scope" => $scopes,
            "user_id" => session('auth_user_id')
        );
        return array_merge($code, [
            'encrypted' => safe_encrypt(json_encode($code), env('JWT_SECRET'))
        ]);
    }

    public static function decode($code){
        try {
            return json_decode(safe_decrypt(rawurldecode($code), env('JWT_SECRET')));
        }catch (\Exception $e){

        }
        return null;
    }
}