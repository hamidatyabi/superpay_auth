<?php


namespace app\contracts;


use app\exceptions\EndpointException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class Mail
{
    public static function send($title, $receiver, $body, $alt_body = ''){
        $mail = new PHPMailer(true);
        try {
            $mail->SMTPDebug = SMTP::DEBUG_OFF;
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST');
            $mail->SMTPAuth   = true;
            $mail->Username   = env('MAIL_USERNAME');
            $mail->Password   = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Port       = env('MAIL_PORT');
            $mail->setFrom(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            $mail->addAddress($receiver);
            $mail->isHTML(true);
            $mail->Subject = $title;
            $mail->Body    = $body;
            $mail->AltBody = $alt_body;
            $mail->send();
        }catch (\Exception $exception){
            throw new EndpointException("send_email_exception", $mail->ErrorInfo);
        }
    }
}