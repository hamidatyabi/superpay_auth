<?php


namespace app\contracts;


use Firebase\JWT\JWT;

class JwtAuth
{
    protected JWT $jwt;
    /**
     * JwtAuth constructor.
     */
    public function __construct()
    {
        $this->jwt = new JWT();
    }

    public function build($scope, $client, $has_refresh = false, $user = null){
        $scope = str_replace(", ", ",", $scope);
        $scopes = ($scope && $scope != "")?explode(",", $scope):[];
        $access_token = array(
            "scope"  => $scopes,
            "aud"  => "",
            'iat'  => time(),
            'nbf'  => time(),
            'exp'  => (time() + $client->access_token_validity),
            "jti" => guidv4(),
            'client_id' => $client->client_id
        );
        if($user){
            $access_token = array_merge($access_token, array(
                'user_id' => $user->id,
                'email' => $user->email
            ));
        }
        $result['access_token'] = $this->jwt::encode($access_token, env('JWT_SECRET'), 'HS256');
        $result['token_type'] = "bearer";
        if($has_refresh){
            $access_token['exp'] = (time() + $client->refresh_token_validity);
            $refresh_token = array_merge($access_token, array(
                "ati" => $access_token['jti']
            ));
            $result['refresh_token']  = $this->jwt::encode($refresh_token, env('JWT_SECRET'), 'HS256');
        }
        $result['expires_in'] = $client->access_token_validity;
        $result['scope'] = implode(",", $scopes);
        $result['jti'] = $access_token['jti'];
        return $result;
    }

    public function decode($token){
        $data = null;
        try {
            return $this->jwt::decode($token, env('JWT_SECRET'), array('HS256'));
        } catch (\Throwable $e) {
            return null;
        }
        return $data;
    }
}