<?php


namespace app\contracts;


class Scopes
{
    public const USER_READ_PRIVATE = "user-read-private";


    public static function get(){
        return [
            self::USER_READ_PRIVATE,
        ];
    }
}