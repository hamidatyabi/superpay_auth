<?php
namespace app;

class Response
{
    public const STATUS_OK = 200;
    public const STATUS_UNAUTHORIZED = 401;
    public const STATUS_ACCESS_DENIED = 403;
    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_INTERNAL_SERVER_ERROR = 500;

    public static function json($data, $status = self::STATUS_OK){
        return response()->data(json_encode($data))->code($status)->contentType("application/json");
    }
}
