<?php
namespace app\exceptions;

use app\Response;

class EndpointException extends \RuntimeException
{
    protected $error;
    protected $status;

    public function __construct($error = 'Unauthorized', $error_description = '', $status = Response::STATUS_BAD_REQUEST)
    {
        $this->error  = ['error' => $error, 'error_description' => $error_description];
        $this->status = $status;
    }
    public function getError()
    {
        return $this->error;
    }
    public function getStatus()
    {
        return $this->status;
    }
}