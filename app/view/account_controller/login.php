<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.css">

    <link rel="stylesheet" href="assets/style.css">
    <title>Login Form</title>
</head>
<body>
<div class="container">
    <div id="logreg-forms">
        <form  action="{:url('login-action', ['continue' => $continue])}" method="post">
            <h1 class="h3 mb-10 font-weight-normal" style="text-align: center"> Login</h1>
            <?php if(@$error): ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
            <?php endif; ?>
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="" autofocus="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <button class="btn btn-success btn-block" type="submit">Login</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="assets/script.js"></script>
</body>
</html>