<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.css">

    <link rel="stylesheet" href="assets/style.css">
    <title>Login Form</title>
</head>
<body>
<div class="container">
    <div class="token">
        <div class="form-group">
            <div class="">Access Token:</div>
            <input type="text" name="access_token" readonly value="" />
            <div class="">Refresh Token:</div>
            <input type="text" name="refresh_token" readonly value="" />
        </div>
        <div class="form-group row ">
            <div class="col-md-6">
                <input type="text"  class="form-control" name="client_id" placeholder="client_id" value="2db3dae0147644d856b4e570867896" required="" autofocus="" />
            </div>
            <div class="col-md-6">
                <input type="text"  class="form-control" name="client_secret" placeholder="client_secret" value="7ceab3ac0494cca16dc0fa5d04f3ba60" required="" autofocus="" />
            </div>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#token">Get Token</a></li>
        <li><a data-toggle="tab" href="#profile" class="profile-tab" data-action="{:url('profile')}">Profile</a></li>
        <li><a data-toggle="tab" href="#refresh_token" class="refresh-token-tab" data-action="{:url('token-api')}">Refresh Token</a></li>
        <li><a data-toggle="tab" href="#client_token" class="client-token-tab" data-action="{:url('token-api')}">Client Token</a></li>
        <li><a data-toggle="tab" href="#register" class="register-tab">Register</a></li>

    </ul>

    <div class="tab-content">
        <div id="token" class="tab-pane fade in active">
            <div id="logreg-forms">
                <form class="form-access-token" action="{:url('token-api')}" method="post">
                    <h1 class="h3 mb-10 font-weight-normal" style="text-align: center"> Get Token</h1>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="" autofocus="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-block" type="submit">Get Token</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <pre id="token-result">

                            </pre>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="profile" class="tab-pane fade">
            <div id="logreg-forms">
                <form class="" action="" method="post">
                    <h1 class="h3 mb-10 font-weight-normal" style="text-align: center"> Profile</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <pre id="profile-result">

                            </pre>
                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div id="refresh_token" class="tab-pane fade">
            <div id="logreg-forms">
                <form class="" action="" method="post">
                    <h1 class="h3 mb-10 font-weight-normal" style="text-align: center"> Refresh Token</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <pre id="refresh-result">

                            </pre>
                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div id="client_token" class="tab-pane fade">
            <div id="logreg-forms">
                <form class="" action="" method="post">
                    <h1 class="h3 mb-10 font-weight-normal" style="text-align: center"> Client Token</h1>
                    <div class="row">
                        <div class="col-md-12">
                            <pre id="client-result">

                            </pre>
                        </div>
                    </div>
                </form>

            </div>

        </div>
        <div id="register" class="tab-pane fade">
            <div id="logreg-forms">
                <form class="register-form" action="{:url('register-api')}" method="post">
                    <h1 class="h3 mb-10 font-weight-normal" style="text-align: center">Register</h1>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="text" id="inputEmail" class="form-control" name="name" placeholder="Name" required="" autofocus="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="" autofocus="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="">
                        </div>
                        <div class="col-md-6">
                            <input type="password" id="inputPassword" name="password_confirmation" class="form-control" placeholder="Password Confirmation" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <button class="btn btn-success btn-block" type="submit">Register</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <pre id="register-result">

                            </pre>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.all.min.js"></script>
<script src="assets/script.js"></script>
</body>
</html>