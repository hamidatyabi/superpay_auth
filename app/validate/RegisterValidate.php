<?php
declare (strict_types = 1);

namespace app\validate;

use app\repositories\UserRepositories\UserRepositoryInterface;
use think\Validate;

class RegisterValidate extends Validate
{

    /**
     * @var array
     */
    protected $rule = [
        'name' => ['require', 'min:5', 'max:191'],
        'email' => ['require', 'email'],
        'password' => ['require', 'confirm:password_confirmation', 'passes'],
        'password_confirmation' => ['require']
    ];

    /**
     * @var array
     */
    protected $message = [
        'name.require' => "The name is mandatory",
        'name.min' => "The name must be 5-191 characters",
        'email.require' => "The email is mandatory",
        'email.email' => "The email format is incorrect",
        'password.require' => "The password is mandatory",
        'password.confirm' => 'The Password and Password Confirmation must be same',
        'password_confirmation.require' => "The password_confirmation is mandatory",
        'password.passes' => 'The password must be 6–30 characters, and include a number, a symbol, a lower and a upper case letter',
    ];

    public function passes($value)
    {
        return (bool) preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@()$%^&*=_{}[\]:;\"'|\\<>,.\/~`±§+-]).{6,30}$/", $value);
    }
}
