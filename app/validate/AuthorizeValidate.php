<?php
declare (strict_types = 1);

namespace app\validate;

use app\contracts\GrantTypes;
use think\Validate;

class AuthorizeValidate extends Validate
{

    /**
     *
     * @var array
     */
    protected $rule = [
        'response_type' => ['require', 'checkResponseType'],
        'client_id' => ['require'],
        'redirect_uri' => ['require', 'url'],
        'scope' => ['checkScope']
    ];

    /**
     *
     * @var array
     */
    protected $message = [
        'response_type.require' => "The response_type is mandatory",
        'response_type.checkResponseType' => "response_type must be code or token",
        'client_id.require' => "The client_id is mandatory",
        'redirect_uri.require' => "The redirect_uri is mandatory",
        'redirect_uri.url' => "The redirect_uri must be url",
        'scope.checkScope' => "Unsupported scope",
    ];

    public function checkResponseType($value) {
        if(strtolower($value) == "code" || strtolower($value) == "token") return true;
        return false;
    }

    public function checkScope($value) {
        if($value == null || $value == "") return true;
        $scopes = explode(",", $this->request->getPrinciple()->getClient()->scope);
        if(array_in_array(explode(",", $value), $scopes)) return true;
        return false;
    }

}
