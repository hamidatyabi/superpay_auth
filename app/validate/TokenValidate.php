<?php
declare (strict_types = 1);

namespace app\validate;

use app\contracts\GrantTypes;
use app\contracts\Principle;
use think\Validate;

class TokenValidate extends Validate
{
    /**
     *
     * @var array
     */
    protected $rule = [
        'grant_type' => ['require', 'checkGrantType']
    ];

    /**
     *
     * @var array
     */
    protected $message = [
        'grant_type.require' => "The grant_type is mandatory",
        'grant_type.checkGrantType' => "Unsupported grant type",
    ];

    public function checkGrantType($value) {
        $authorized_grant_types = explode(",", $this->request->getPrinciple()->getClient()->authorized_grant_types);
        if(in_array(strtolower($value), GrantTypes::get()) && in_array(strtolower($value), $authorized_grant_types)) return true;
        return false;
    }


}
