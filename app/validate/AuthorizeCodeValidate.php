<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class AuthorizeCodeValidate extends Validate
{
    /**
     *
     * @var array
     */
    protected $rule = [
        'code' => ['require']
    ];

    /**
     *
     * @var array
     */
    protected $message = [
        'code.require' => "The code is mandatory",
    ];

}
