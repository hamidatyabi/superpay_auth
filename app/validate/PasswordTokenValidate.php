<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class PasswordTokenValidate extends Validate
{
    /**
     * @var array
     */
    protected $rule = [
        'email' => ['require', 'email'],
        'password' => ['require'],
        'scope' => ['checkScope']
    ];

    /**
     * @var array
     */
    protected $message = [
        'email.require' => "The email is mandatory",
        'email.email' => "The email format is incorrect",
        'password.require' => "The password is mandatory",
        'scope.checkScope' => "Unsupported scope",
    ];

    public function checkScope($value) {
        if($value == null || $value == "") return true;
        $scopes = explode(", ", $this->request->getPrinciple()->getClient()->scope);
        if(array_in_array(explode(", ", $value), $scopes)) return true;
        return false;
    }
}
