<?php
declare (strict_types = 1);

namespace app;

use app\contracts\JwtAuth;
use app\repositories\CacheRepositories\CacheRepository;
use app\repositories\CacheRepositories\RedisCacheRepository;
use app\repositories\ClientRepositories\ClientRepositoryInterface;
use app\repositories\ClientRepositories\ClientRepositoryMysql;
use app\repositories\UserRepositories\UserRepositoryInterface;
use app\repositories\UserRepositories\UserRepositoryMysql;
use think\Service;

/**
 * 应用服务类
 */
class AppService extends Service
{
    public function register()
    {
        // 服务注册
    }

    public function boot()
    {
        $this->app->bind(CacheRepository::class, RedisCacheRepository::class);
        $this->app->bind(ClientRepositoryInterface::class, ClientRepositoryMysql::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepositoryMysql::class);

        // 服务启动
    }
}
