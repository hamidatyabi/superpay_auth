<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
use \app\middleware\BearerAuthorizationMiddleware;
use \app\middleware\BasicAuthorizationMiddleware;
use \app\contracts\Scopes;
use \app\contracts\ScopeAccess;
use \app\middleware\AuthenticationMiddleware;

Route::get("/me", "ProfileController/me")->middleware(BearerAuthorizationMiddleware::class, null, [ScopeAccess::USER])->name("profile");
Route::get("/authorize", "AuthorizeController/authorize")->middleware(AuthenticationMiddleware::class);
Route::get("/", "EndpointsController/index")->name("endpoints");
Route::get("/user/verification", "RegisterController/verification")->name("user-verification");

Route::get("/profile", "AccountController/profile")->name("user-profile")->middleware(AuthenticationMiddleware::class);
Route::get("/login", "AccountController/login")->name("login");
Route::post("/login", "AccountController/login")->name("login-action");
Route::post("/logout", "AccountController/logout")->name("logout");

Route::group("oauth", function (){
    Route::post('token', 'TokenController/token')->middleware(BasicAuthorizationMiddleware::class)->name("token-api");
    Route::post('register', 'RegisterController/register')->middleware(BasicAuthorizationMiddleware::class)->name("register-api");
});