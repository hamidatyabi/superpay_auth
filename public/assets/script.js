
$('.form-access-token').submit(function (e){
    e.preventDefault();
    var formData = $(this);

    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: {
            email: $('.form-access-token input[name="email"]').val(),
            password: $('.form-access-token input[name="password"]').val(),
            grant_type: "password",
            scope: ""
        },
        headers: {
            'Authorization': 'Basic '+ btoa($('input[name="client_id"]').val()+":"+$('input[name="client_secret"]').val())
        },
        dataType: "json"
    }).done(function(response) {
        Swal.fire({
            title: 'Success',
            text: 'You Got Token Successfully',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then(result => {
            $('#token-result').html(JSON.stringify(response, undefined, 4));
            $('input[name="access_token"]').val(response.access_token);
            $('input[name="refresh_token"]').val(response.refresh_token);
        })

    }).fail(function(data) {
        $('#token-result').html(JSON.stringify(data.responseJSON, undefined, 4));
        Swal.fire({
            title: data.responseJSON.error,
            text: data.responseJSON.error_description,
            icon: 'error',
            confirmButtonText: 'OK',
        })

    });
});
$('.register-form').submit(function (e){
    e.preventDefault();
    var formData = $(this);

    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: {
            name: $('.register-form input[name="name"]').val(),
            email: $('.register-form input[name="email"]').val(),
            password: $('.register-form input[name="password"]').val(),
            password_confirmation: $('.register-form input[name="password_confirmation"]').val()
        },
        headers: {
            'Authorization': 'Basic '+ btoa($('input[name="client_id"]').val()+":"+$('input[name="client_secret"]').val())
        },
        dataType: "json"
    }).done(function(response) {
        Swal.fire({
            title: 'Success',
            text: 'You Got Token Successfully',
            icon: 'success',
            confirmButtonText: 'OK',
        }).then(result => {
            $('#register-result').html(JSON.stringify(response, undefined, 4));
        })

    }).fail(function(data) {
        $('#register-result').html(JSON.stringify(data.responseJSON, undefined, 4));
        Swal.fire({
            title: data.responseJSON.error,
            text: data.responseJSON.error_description,
            icon: 'error',
            confirmButtonText: 'OK',
        })

    });
});
$('.profile-tab').on('click', function (e){
    $.ajax({
        method: "GET",
        url: $(this).data('action'),
        data: {},
        headers: {
            'Authorization': 'Bearer '+ $('input[name="access_token"]').val()
        },
        dataType: "json"
    }).done(function(response) {
        $('#profile-result').html(JSON.stringify(response, undefined, 4));
    }).fail(function(data) {
        $('#profile-result').html(JSON.stringify(data.responseJSON, undefined, 4));

    });
});
$('.refresh-token-tab').on('click', function (e){
    $.ajax({
        method: "POST",
        url: $(this).data('action'),
        data: {
            grant_type: "refresh_token",
            refresh_token: $('input[name="refresh_token"]').val()
        },
        headers: {
            'Authorization': 'Basic '+ btoa($('input[name="client_id"]').val()+":"+$('input[name="client_secret"]').val())
        },
        dataType: "json"
    }).done(function(response) {
        $('#refresh-result').html(JSON.stringify(response, undefined, 4));
    }).fail(function(data) {
        $('#refresh-result').html(JSON.stringify(data.responseJSON, undefined, 4));
    });
});
$('.client-token-tab').on('click', function (e){
    $.ajax({
        method: "POST",
        url: $(this).data('action'),
        data: {
            grant_type: "client_credentials"
        },
        headers: {
            'Authorization': 'Basic '+ btoa($('input[name="client_id"]').val()+":"+$('input[name="client_secret"]').val())
        },
        dataType: "json"
    }).done(function(response) {
        $('#client-result').html(JSON.stringify(response, undefined, 4));
    }).fail(function(data) {
        $('#client-result').html(JSON.stringify(data.responseJSON, undefined, 4));
    });
});